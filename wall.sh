#!/bin/bash

res="$(java TimeDivider.java 16)"

rootpath="/home/marius/scripts/mojave"

# build a path from the number
path="$rootpath/$res.jpeg"

# set the wallpaper
DISPLAY=:0 feh --bg-scale "$path" &

# set an image for the lockscreen to use
convert -resize 1920x1080\! $path "$rootpath/lock.png"
