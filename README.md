This is a short script I wrote for myself to get the wallpaper effect from macOS Mojave.

I own none of these pictures. I got them from [Cult of Mac](https://www.cultofmac.com/553577/grab-all-16-macos-mojave-dynamic-wallpapers-right-here/)

You can substitute any other 16 pictures, just name them `/\d{1,2}\.jpeg/`.

Then run this scrip from a cron job every half hour or so.
