import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

public class TimeDivider{
    public static void main(String[] args) {
        if(args.length != 1){
            System.out.println("Usage: supply the number of segments a day should be divided into.");
            System.exit(1);
        }
        double divisor = Integer.parseInt(args[0]);
        int secondOfDay = LocalTime.now().toSecondOfDay();
        int daySlice = (int)(secondOfDay / (double)TimeUnit.DAYS.toSeconds(1) * divisor);
        System.out.println(daySlice);
    }
}